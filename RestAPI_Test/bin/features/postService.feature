Feature: To place an order for pet
  I want to order a pet in pet store

  Scenario: To place an order of pet
    Given I set Pet details api endpoint
    When I set post request header
    And I send the post HTTP request with name "Abarna" job "Tester"
    Then I receive valid response "201"  
    
    
   Scenario: To place an invalid order of pet
    Given I set Pet details api endpoint
    When I set post request header
    And I send the post HTTP request with name "Abarna" job "Tester"
    Then I receive invalid post response "200"