Feature: To delete an purchase of a pet
  I want to delete an purchase order by ID

  Scenario: To delete an purchase order by ID
    Given I set Pet details api endpoint
    When I set delete request header
    And I send the delete HTTP request with order Id "2"
    Then I receive valid response "204"  
    
    
  Scenario: To delete an purchase order by invalid ID
    Given I set Pet details api endpoint
    When I set delete request header
    And I send the delete HTTP request with order Id "100"
    Then I receive invalid delete response "200" 