Feature: To get pet inventory by status
  I want to get the pet store inventories by status

  Scenario: To get pet inventories by status
    Given I set Pet details api endpoint
    When I set get request header
    And I send the get HTTP request
    Then I receive valid response "200"
    
  Scenario: To get purchase order by order ID
    Given I set Pet details api endpoint
    When I set get request header
    And I send the get HTTP request with order ID "2"
    Then I receive valid response "200"
    
  Scenario: To get invalid purchase order by order ID
    Given I set Pet details api endpoint
    When I set get request header
    And I send the get HTTP request with order ID "15"
    Then I receive valid get response by OrderId