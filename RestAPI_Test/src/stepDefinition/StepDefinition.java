package stepDefinition;

import org.json.simple.JSONObject;
import org.testng.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class StepDefinition {
	
	RequestSpecification httpRequest;
	Response response;
	String request;
	int orderId;
	
	@Given("^I set Pet details api endpoint$")
	 public void setBaseURI(){
		RestAssured.baseURI = "https://reqres.in/";
		httpRequest = RestAssured.given();
	 }
	
	@When("^I set get request header$")
	 public void setGetRequest(){
		request = "/api/users";
	 }
	
	@And("^I send the get HTTP request$")
	 public void sendGetRequest(){
		response = httpRequest.request(Method.GET, request+"?page=2");
	 }
	
	@And("^I send the get HTTP request with order ID \"(.*)\"$")
	 public void sendGetRequestByOrderId(int orderId){
		response = httpRequest.request(Method.GET, request+"/"+orderId);
	 }
	
	@Then("^I receive valid response \"(.*)\"$")
	 public void validateGetResponse(int code){
		String responseBody = response.getBody().asString();
		Assert.assertTrue(response.getStatusCode() == code, responseBody);
	 }
	
	@Then("^I receive valid get response by OrderId$")
	 public void validateGetResponseByOrderId(){
		String responseBody = response.getBody().asString();
		Assert.assertFalse(response.getStatusCode() == 200, responseBody);
	 }
	
	@When("^I set post request header$")
	 public void setPostRequest(){
		request = "/api/users";
	 }
	
	@And("^I send the post HTTP request with name \"(.*)\" job \"(.*)\"$")
	 public void sendPostRequest(String name, String job){
		RequestSpecification request = RestAssured.given();
		JSONObject requestParams = new JSONObject();
		requestParams.put("name", name);  
		requestParams.put("job", job);
		request.body(requestParams.toJSONString());
		response = request.post("/api/users");
	 }
	
	@Then("^I receive invalid post response \"(.*)\"$")
	 public void validatePostResponse(int code){
		String responseBody = response.getBody().asString();
		Assert.assertFalse(response.getStatusCode() == code, responseBody);
	 }
	
	@When("^I set delete request header$")
	 public void setDeleteRequest(){
		request = "/api/users";
	 }
	
	@And("^I send the delete HTTP request with order Id \"(.*)\"$")
	 public void sendDeleteRequestByOrderId(int orderId){
		RequestSpecification reqest = RestAssured.given();
		reqest.header("Content-Type", "application/json");
		response = reqest.delete(request+"/"+orderId);
	 }
	
	@Then("^I receive invalid delete response \"(.*)\"$")
	 public void validateDeleteResponse(int code){
		Assert.assertFalse(response.getStatusCode() == code, "Success");
	 }
}
